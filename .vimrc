:syntax on
":set background=dark

:if &term == "xterm"
:  set t_kb=
:  fixdel
:  set backspace=2 " allow backspace beyond insertion point
:  set ic                  " ignore case on search
:  set laststatus=2        " always show status line
: set undolevels=1000     " large undo history
:endif
:set tabstop=4
:set shiftwidth=4
:set expandtab



" Custom keybindings
:map <F4> <Esc>!}fmt<CR>
:map <F2> <Esc>:setlocal spell spelllang=en_us<CR>
:map <F3> <Esc>:setlocal nospell<CR>
" V runs ispell
map V :w^M:!ispell -x %^M:e!^M^M
au BufRead,BufNewFile *.json set filetype=json
au! Syntax json source $HOME/.vim/plugins/json.vim
cmap w!! w !sudo tee % >/dev/null
