alias dockerenv='eval $(docker-machine env default)'
alias dockerstart='docker-machine start default && eval "$(docker-machine env default)"'
alias dockerstop='docker-machine stop default'
